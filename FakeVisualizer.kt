import androidx.compose.animation.core.*
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

@Composable
fun FakeVisualizerContent() {
    val infiniteTransition = rememberInfiniteTransition(label = "infinite transition")
    val initialScale = remember { Random.nextFloat() * (7f - 3f) + 3f }
    val scale by infiniteTransition.animateFloat(
        initialValue = initialScale,
        targetValue = 9f,
        animationSpec = infiniteRepeatable(tween(800), RepeatMode.Reverse),
        label = "scale"
    )

    val numBars = 120
    val barWidth = 4f

    val bars = remember {
        List(numBars) { index ->
            val angle = index * (360f / numBars)
            val originalHeight = (-20..-10).random().toFloat()
            Bar(angle, originalHeight, barWidth)
        }
    }
    Canvas(modifier = Modifier.background(Color.Black).fillMaxSize()) {
        val centerX = size.width / 2f
        val centerY = size.height / 2f
        val radius = size.width.coerceAtMost(size.height) / 4f

        bars.forEach { bar ->
            val startX = centerX + radius * cos(bar.angle.toRadians())
            val startY = centerY + radius * sin(bar.angle.toRadians())
            val endX = (radius + bar.height * scale) * cos(bar.angle.toRadians())
            val endY = (radius + bar.height * scale) * sin(bar.angle.toRadians())

            drawLine(
                color = Color.White,
                start = Offset(startX, startY),
                end = Offset(startX + endX, startY + endY),
                strokeWidth = bar.width,
                cap = androidx.compose.ui.graphics.StrokeCap.Round
            )
        }
    }
}

data class Bar(val angle: Float, val height: Float, val width: Float)

fun Float.toRadians() = this.toDouble().toRadians()

fun Double.toRadians() = Math.toRadians(this).toFloat()

